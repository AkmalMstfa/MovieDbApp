# The Movie DB Mobile

## Getting started

First you'll have to clone the App:

```bash
git clone https://gitlab.com/AkmalMstfa/MovieDbApp.git
```

Then, inside the project's folder, install it's dependencies.

With Yarn:

```bash
yarn
```

or npm:

```bash
npm install
```

## Settings

### API's and keys

You have to configure your API endpoint and API KEY in `env.json` file in project folder.

```json
{
  "APP_NAME": "",
  "MOVIEDB_API_KEY": "",
  "MOVIEDB_READ_ACCESS_TOKEN": "",
  "MOVIEDB_API_BASE_URL": "",
  "YOUTUBE_API_KEY": ""
}
```

| Attribute                 | Description                                                                                                                        |
| ---------------           | ---------------------------------------------------------------------------------------------------------------------------------- |
| APP_NAME                  | Name of the app (show in the login page and header.)                                                                               |
| MOVIEDB_API_KEY           | Moviedb api key generated [How do I get one?](https://developers.themoviedb.org/4/getting-started/introduction)                    |
| MOVIEDB_READ_ACCESS_TOKEN | Moviedb read access roken generated [How do I get one?](https://developers.themoviedb.org/4/getting-started/introduction)|
| MOVIEDB_API_BASE_URL      | Moviedb base url to make requests. Defaults to: `https://api.themoviedb.org/4/`                                                    |
| YOUTUBE_API_KEY           | Google API and Youtube service enabled key. [How do I get one?](https://developers.google.com/youtube/v3/getting-started?hl=pt-br) |

## Running

Now you can run the App with `react-native run-android` or `react-native run-ios` depending on your platform.

## Made with

- [React Native typescript template](https://github.com/react-native-community/react-native-template-typescript)
- [React Native Firebase](https://rnfirebase.io/) (Authentication and favorite movies)
- [React Native Paper](https://callstack.github.io/react-native-paper/index.html) (UI components)
- [React Native Youtube](https://github.com/inProgress-team/react-native-youtube) (Video component for trailers)
- [React Navigation 4.x.x](https://reactnavigation.org/en/) (Navigation)
- [Restful React](https://github.com/contiamo/restful-react) (This is amazing. Using API like hooks) :heart:
- [Formik](https://jaredpalmer.com/formik/docs/overview) (Handling forms)
- [yup](https://github.com/jquense/yup) (Validating forms)
- [Styled Components](https://www.styled-components.com/) (Make things beautiful)


