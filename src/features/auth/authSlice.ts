import {createSlice} from '@reduxjs/toolkit';

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    logged: false,
    token: '',
  },
  reducers: {
    login: (state, {payload}) => {
      state.logged = false;
      state.token = payload;
    },
    logout: (state) => {
      state.logged = false;
    },
  },
});

// Action creators are generated for each case reducer function
export const {login, logout} = authSlice.actions;

export default authSlice.reducer;
