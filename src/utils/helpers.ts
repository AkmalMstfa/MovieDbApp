import * as Yup from 'yup';

export function GetErrorMessage(error: {
  message: string;
  code: string;
}): string {
  switch (error.code) {
    case 'auth/user-not-found':
      return 'User not found.';
    case 'auth/user-disabled':
      return 'This user has been disabled.';
    case 'auth/invalid-email':
      return 'Invalid email.';
    case 'auth/wrong-password':
      return 'Incorrect password.';
    case 'auth/email-already-in-use':
      return 'An account already exists for this email.';
    case 'auth/operation-not-allowed':
      return 'User creation failed. Please contact us +6282218328450.';
    case 'auth/weak-password':
      return 'Password quite vulnerable. Please choose another password.';
    default:
      return 'An unexpected error has occurred. Please check your internet connection.';
  }
}

export const LoginValidationSchema: Yup.ObjectSchema<
  Yup.Shape<
    object,
    {
      email: string;
      password: string;
    }
  >
> = Yup.object().shape({
  email: Yup.string().email('Invalid email.').required('Fill this field'),
  password: Yup.string()
    .min(6, 'Your password must be longer than 6 characters')
    .required('Fill this field'),
});

export const RegisterValidationSchema: Yup.ObjectSchema<
  Yup.Shape<
    object,
    {
      email: string;
      password: string;
      passwordConfirmation: string;
    }
  >
> = Yup.object().shape({
  email: Yup.string().email('Invalid email.').required('Fill this field'),
  password: Yup.string()
    .min(6, 'Your password must be longer than 6 characters')
    .required('Fill this field'),
  passwordConfirmation: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must be the same')
    .required('Fill this field'),
});
