import React from 'react';
import {StatusBar} from 'react-native';
import {Provider as PaperProvider} from 'react-native-paper';
import {Provider, useSelector} from 'react-redux';
import {RestfulProvider} from 'restful-react';
import ENV from '../env.json';
import ErrorBoundary from './components/error-boundary';
import Router from './routes';
import store, {RootState} from './store';
import theme from './styles/theme';

const App = () => {
  // const auth = useSelector((state: RootState) => state.auth);
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor={theme.colors.white} />
      <PaperProvider theme={theme}>
        <ErrorBoundary>
          <RestfulProvider
            queryParams={{api_key: ENV.MOVIEDB_API_KEY}}
            requestOptions={() => ({
              headers: {
                Authorization: `Bearer ${ENV.MOVIEDB_READ_ACCESS_TOKEN}`,
                'Content-Type': 'application/json;charset=utf-8',
              },
            })}
            base={ENV.MOVIEDB_API_BASE_URL}>
            <Router />
          </RestfulProvider>
        </ErrorBoundary>
      </PaperProvider>
    </>
  );
};

export default App;
